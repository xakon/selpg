SHELL	:= /bin/sh

CFLAGS	:= -Wall

TARGET	:= selpg

.PHONY: all debug test release clean cleanall veryclean

all: debug
clean:
	-rm -f *.o
cleanall: clean
	-rm -f ${TARGET}
veryclean: cleanall

debug release test: ${TARGET}
debug: CFLAGS += -g -DDEBUG
release: CPPFLAGS += -DNDEBUG
